//variables intitlised
int buttonPins[] = {RX,D1,D6,D2,A0};
bool anyPressed = false;
bool backPressed = false;
bool pausePressed = false;
bool forwardPressed = false;
bool functionPressed = false;
int lastVolumeReading =0;

//include libaries used
#include <ESP8266WiFi.h> 


//define interupts for button presses
void ICACHE_RAM_ATTR back() {
    anyPressed = true; 
    backPressed = true;
    Serial.println("Back button pressed");
}

void ICACHE_RAM_ATTR pause() {
    anyPressed = true;
    pausePressed = true;
    Serial.println("Pause button pressed");
}

void ICACHE_RAM_ATTR forward() {
    anyPressed = true;
    forwardPressed = true;
    Serial.println("Forward button pressed");
}

void ICACHE_RAM_ATTR function() {
    anyPressed = true;
    functionPressed = true;
    Serial.println("Function button pressed");
}

//runs at start up before loop
void setup() {
    Serial.begin(115200);
    //set up buttons
    for(int i = 0 ; i < 6; i++){
        pinMode(buttonPins[i],INPUT_PULLUP);
    }
    attachInterrupt(buttonPins[0], back, FALLING);
    attachInterrupt(buttonPins[1], pause, FALLING);
    attachInterrupt(buttonPins[2], forward, FALLING);
    attachInterrupt(buttonPins[3], function, FALLING);


    connectToWifi();
}
        
//loops untill connected to a wifi network
void connectToWifi() {
  bool connected = false;
  const int numNetworks = 2;
  String networkInfo[numNetworks][2] = { //array of wifi netowrks. it will check the ones at the top first
    {"Galaxy A1272C7", "yavh5351"},
    {"TALKTALK978EBA", "BPN4F9V9"},
  };
  while (connected== false){
    int numOfNetworks = WiFi.scanNetworks();
    Serial.print(numOfNetworks);
    Serial.println(" networks found");
    for (int i = 0; i < numOfNetworks; ++i) {
      for (int j = 0; j<numNetworks; ++j) {
        if (WiFi.SSID(i) == networkInfo[j][0] && connected == false) {
          Serial.println("match found");
          Serial.println(networkInfo[j][0]);
          WiFi.begin(networkInfo[j][0],networkInfo[j][1]);
          int timeout = 40; // Wait 10 seconds 40*250 milli seconds
          while (WiFi.status() != WL_CONNECTED && (timeout-- > 0)) {
          delay(250);
          Serial.print(".");
          }
          Serial.println("");
          if (WiFi.status() == WL_CONNECTED) {
            connected = true;
            Serial.println("sucessfully connected to");
            Serial.println(networkInfo[j][0]);
          }
        }
      }
    }
  }

}
// runs forever
void loop() {
  if(anyPressed) {
    if(backPressed) {
        // Add code to handle back button press
        backPressed = false;
    }
    if(pausePressed) {
        // Add code to handle pause button press
        pausePressed = false;
    }
    if(forwardPressed) {
        // Add code to handle forward button press
        forwardPressed = false;
    }
    if(functionPressed) {
        // Add code to handle volume change
        functionPressed = false;
    }
    anyPressed = false;
  }
    //checks to see if volume changed
  int volumeReading = map(analogRead(A0),0,1000,25,0);
  if(volumeReading != lastVolumeReading) {
    Serial.print("Volume changed to: ");
    Serial.println((volumeReading)*4);
    lastVolumeReading = volumeReading;
  }
}